package monopoly;

import java.util.Random;
import java.util.Scanner;

public class main {

	public static Scanner in = new Scanner(System.in);
	public static Random rn = new Random();
	
	public static void main(String[] args) {
		
		Street etsel = new Street("ha'etsel",2, 60, 20, 100, 300, 900, 1600, 2500, 500, 2500, 300);
		Street shazar = new Street("shazar blvd",4, 60, 40, 200, 600, 1800, 3200, 4500, 500, 2500, 300);
		Street herzel = new Street("herzel blvd",7, 100, 60, 300, 900, 2700, 4000, 5500, 500, 2500, 500);
		Street hatayelet = new Street("hatayelet",9, 100, 60, 300, 900, 2700, 4000, 5500, 500, 2500, 500);
		Street bengu = new Street("ben gurion blvd",10, 100, 80, 400, 1000, 3000, 4500, 6000, 500, 2500, 600);
		Street vietzman = new Street("vietzman blvd",12, 140, 100, 500, 1500, 4500, 6250, 7500, 1000, 5000, 700);
		Street hamelachim = new Street("hamelachim",14, 140, 100, 500, 1500, 4500, 6250, 7500, 1000, 5000, 700);
		Street raynes = new Street("raynes",15, 160, 120, 600, 1800, 5000, 7000, 9000, 1000, 5000, 800);
		Street hanadiv = new Street("hanadiv blvd",17, 180, 140, 700, 2000, 5500, 7500, 9500, 1000, 5000, 900);
		Street sweed = new Street("shvedya st",19, 180, 140, 700, 2000, 5500, 7500, 9500, 1000, 5000, 900);
		Street nof = new Street("yafe nof st",20, 200, 160, 800, 2200, 6000, 8000, 10000, 1000, 5000, 1000);
		Street benye = new Street("ben yehuda st",22, 220, 180, 900, 2500, 7000, 8750, 10500, 1500, 7500, 1100);
		Street george = new Street("king george st",24, 220, 180, 900, 2500, 7000, 8750, 10500, 1500, 7500, 1100);
		Street jaffa = new Street("",25, 240, 200, 1000, 3000, 7500, 9250, 11000, 1500, 7500, 1200);
		Street bialik = new Street("bialik",27, 260, 220, 1100, 3300, 8000, 9750, 11500, 1500, 7500, 1300);
		Street jabutinsky = new Street("jabutinsky",28, 260, 220, 1100, 3300, 8000, 9750, 11500, 1500, 7500, 1300);
		Street tzofim = new Street("ma'ale tsofim",30, 280, 240, 1200, 3600, 8500, 10250, 12000, 1500, 7500, 1400);
		Street sokolov = new Street("sokolov",32, 300, 260, 1300, 3900, 9000, 11000, 12750, 2000, 10000, 1500);
		Street eshel = new Street("ha'eshel",33, 300, 260, 1300, 3900, 9000, 11000, 12750, 2000, 10000, 1500);
		Street galey = new Street("galey Tchelet st",35, 320, 280, 1500, 4500, 10000, 12000, 14000, 2000, 10000, 1600);
		Street hayarkon = new Street("hayarkon st",38, 350, 350, 1750, 5000, 11000, 13000, 15000, 2000, 10000, 2000);
		Street roth = new Street("rothchild blvd",40, 400, 500, 2000, 6000, 14000, 17000, 20000, 2000, 10000, 2000);
		
		Town beer7 = new Town("beer 7", etsel, shazar);
		Town ashdod = new Town("ashdod", herzel, hatayelet, bengu);
		Town natanya = new Town ("natanya", vietzman, hamelachim, raynes);
		Town haifa = new Town ("haifa", hanadiv, sweed, nof);
		Town jerusalem = new Town ("jerusalem", benye, george, jaffa);
		Town ramatGan = new Town ("ramat gan", bialik, jabutinsky, tzofim);
		Town herzelya = new Town ("herzelya", sokolov, eshel, galey);
		Town telAviv = new Town ("tel aviv", hayarkon, roth);
		
		Train tlv = new Train("beer 7", 6, 1000, 100, 200, 400, 800);
		Train jeru = new Train("haifa", 16, 1000, 100, 200, 400, 800);
		Train haif = new Train("jerusalem", 26, 1000, 100, 200, 400, 800);
		Train beer = new Train("Tel-Aviv", 36, 1000, 100, 200, 400, 800);

		
	}
	
	public static int rollDice(){
		int die1 = rn.nextInt(6)+1;
		int die2 = rn.nextInt(6)+1;
		System.out.println(die1 + "," + die2);
		int sum = die1+die2;
		return (sum);
	}
	
}
