package monopoly;

public class Town {
	String _townName;
	Street[] _theStreets;
	
	public Town(String a, Street b, Street c){
		_townName = a;
		_theStreets[0] = b;
		_theStreets[1] = c;
	}
	
	public Town(String a, Street b, Street c, Street d){
		_townName = a;
		_theStreets[0] = b;
		_theStreets[1] = c;
		_theStreets[2] = d;
	}
}
