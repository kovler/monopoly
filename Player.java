package monopoly;

public class Player {
	String _playerName;
	int _wallet;
	Street[] _assets;
	int _place;
	boolean _okToGo;
	
	public Player(String a){
		_playerName = a;
		_wallet = 2000;
		_place = 1;
		_assets = new Street[28];
		_okToGo = true;
	}
	
	public void goToJail(){
		this._okToGo = false;
	}
}
