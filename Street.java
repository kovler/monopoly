package monopoly;


public class Street {
	String _placeName;
	int _placeLocation;
	int _price;
	
	int _rent;
	int _rentOne;
	int _rentTwo;
	int _rentThree;
	int _rentFour;
	int _rentHotel;
	int _housePrice;
	int _hotelPrice;
	int _mortgagePrice;
	
	public Street(String a, int b, int c, int d, int e, int f, int g, int h, int i, int j, int k, int l){
		_placeName = a;
		_placeLocation = b;
		_price = c;
		
		_rent = d;
		_rentOne = e;
		_rentTwo = f;
		_rentThree = g;
		_rentFour = h;
		_rentHotel = i;
		_housePrice = j;
		_hotelPrice = k;
		_mortgagePrice = l;
	}
}
